def vowel_swapper(string):
    # ==============
    # Your code here

    # make global for faster execution
    SWAPS = {'a':'4','e':'3','i':'!','o':'ooo','u':'|_|'}
    
    chars = list(string)
    
    for key, value in SWAPS.items():
        for i, c in enumerate(chars):
            if c == 'O':
                chars[i] = '000'
            elif c.lower() == key:
                chars[i] = value
    return ''.join(chars)

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console