def calculator(a, b, operator):
    # ==============
    # Your code here
    
    if operator == '+':
        v = a + b
    elif operator == '-':
        v = a - b
    elif operator == '*':
        v = a * b
    elif operator == '/':
       v = a / b
    else:
        return 'Unknown operator'
        
    # return value as an integer
    return int(v)
    
    # return value as a string of an interger binary
    #return bin(int(v))[2:]

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
