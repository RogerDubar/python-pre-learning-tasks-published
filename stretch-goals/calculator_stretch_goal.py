def calculator(a, b, operator):
    # ==============
    # Your code here
    
    if operator == '+':
        v = a + b
    elif operator == '-':
        v = a - b
    elif operator == '*':
        v = a * b
    elif operator == '/':
       v = a / b
    else:
        return 'Unknown operator'
        
    # return value as an integer
    #return int(v)
    
    # return value as a string of an interger binary
    # remembering the value may be a negative number
    return bin(int(v)).replace('0b','')

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
