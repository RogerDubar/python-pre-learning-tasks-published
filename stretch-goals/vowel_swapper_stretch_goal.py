def vowel_swapper(string):
    # ==============
    # Your code here
    
    # make global for faster execution
    
    # as shown in readme
    #SWAPS = {'a':'4','e':'3','i':'!','o':'ooo','u':'|_|'}
    
    # to match expected output shown below
    SWAPS = {'a':'/\\','e':'3','i':'!','o':'ooo','u':'\/'}
    
    chars = list(string)
    
    for key, value in SWAPS.items():
        matches = 0
        for i, c in enumerate(chars):
            if c.lower() == key:
                matches += 1
                if matches == 2: # look for the second match only
                    if c == 'O':
                        chars[i] = '000'
                    else:
                        chars[i] = value
                    break # we don't want to look for any more matches
    return ''.join(chars)


    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
