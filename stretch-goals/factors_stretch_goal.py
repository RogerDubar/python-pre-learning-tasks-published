def factors(number):
    # ==============
    # Your code here
    
    # create list of factors of x excluding 1 and x
    f = [i for i in range(2,number) if number%i==0]
    
    # Return list of factors, otherwise say it is a prime
    return f or f'{number} is a prime number'
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
 